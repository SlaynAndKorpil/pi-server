use std::net::{TcpStream, TcpListener};
use std::io::{Write, Read};

fn handle_client(mut stream: TcpStream) -> std::io::Result<()> {
    println!("\nstarted new client process");

    let mut buf = [1; 1024];

    stream.read(&mut buf)?;

    let msg = String::from_utf8_lossy(&buf[..]);

    println!("{}", msg);

    let response = 
        if let Some(remainder) = msg.strip_prefix("GET ") {
            let uri = remainder.split_whitespace().nth(0).unwrap();

            let content = match uri {
                "/" => "You have chosen wisely.",
                "/hello_shell" => "Starting shell",
                _ => "I don't know what you mean."
            };

            let html = format!("<html><body>{}</body></html>", content);

            format!("HTTP/1.1 200 OK\t\n{}\r\n{}", "", html)
        } else {
            let html = "<html><body>404 PAGE NOT FOUND</body></html>";

            format!("HTTP/1.1 404 NOT FOUND\t\n\r\n{}", html)
        };

    stream.write(&response.as_bytes())?;
    stream.flush()?;

    Ok(())
}

fn main() -> std::io::Result<()> {
    let port = std::env::args().nth(1).expect("No port specified :(");

    let addr = format!("127.0.0.1:{}", port);

    let listener = TcpListener::bind(&addr)
        .expect(&format!("Could not listen on {}", &addr));

    for stream in listener.incoming() {
        if let Ok(stream) = stream {
            handle_client(stream);
        }
    }

    Ok(())
}
